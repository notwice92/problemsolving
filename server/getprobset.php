<?php
    header('Content-Type: application/json');
    
    require_once 'include/DB_Functions.php';
    $db = new DB_Functions();
    
    $key = $_POST['searchKey'];
    
    $result = $db->getprobset($key);
    if($result != false){
        echo json_encode($result);
    }else{
        echo "error";
    }
    ?>