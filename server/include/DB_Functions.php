<?php
    
    class DB_Functions {
        
        private $db;
        
        //put your code here
        // constructor
        function __construct() {
            require_once 'DB_Connect.php';
            // connecting to database
            $this->db = new DB_Connect();
            $this->db->connect();
        }
        
        // destructor
        function __destruct() {
            
        }
        
        public function register($name, $email, $pw){
            
            $result = mysql_query("INSERT INTO users(name, email, pw) VALUES('$name', '$email', '$pw')");
            if($result){
                return true;
            }
            else{
                return false;
            }
        }
        
        public function login($email, $pw){
            $result = mysql_query("SELECT id, name, email FROM users WHERE email = '$email' AND pw = '$pw' LIMIT 1");
            $no_of_rows = mysql_num_rows($result);
            if ($no_of_rows > 0) {
                return mysql_fetch_assoc($result);
            } else {
                return false;
            }
            
        }

        public function createset( $title, $user_id, $category_id ){
            $result = mysql_query("INSERT INTO problemset(title, writer, category) VALUES('$title', '$user_id', '$category_id')");
            if($result){
                return mysql_insert_id();
            }
            else{
                return false;
            }
        }

        public function getCategories(){
            $result = mysql_query("SELECT * FROM category");
            $no_of_rows = mysql_num_rows($result);
            if ($no_of_rows > 0) {
                $response = array();
                   while($row = mysql_fetch_assoc($result)){
                       $response[] = $row;
                   }
                   return $response;
            } else {
                return false;
            }
        }
        
        public function createproblem($set_id, $content, $img, $sound, $ans, $correct, $tags){
            $result = mysql_query("INSERT INTO problem(contents, image_url, sound_url, solution, correct, set_id, tags) VALUES('$content', '$img', '$sound', '$ans', '$correct', '$set_id', '$tags')");
            if($result){
                return true;
            }
            else{
                return false;
            }
        }
        
        public function getprobset($key){
            $result = mysql_query("SELECT DISTINCT s.*, u.name FROM problemset s, users u WHERE ( u.id = s.writer ) and ( s.title LIKE '%$key%' OR s.category in (select c.id from category c where c.title LIKE '%$key%'))");
            $no_of_rows = mysql_num_rows($result);
            if ($no_of_rows > 0) {
                $response = array();
                while($row = mysql_fetch_assoc($result)){
                    $response[] = $row;
                }
                return $response;
            } else {
                return false;
            }
        }
        
        public function getproblems($set_id){
            $result = mysql_query("SELECT * FROM problem WHERE set_id = '$set_id'");
            $no_of_rows = mysql_num_rows($result);
            if ($no_of_rows > 0) {
                $response = array();
                while($row = mysql_fetch_assoc($result)){
                    $response[] = $row;
                }
                return $response;
            } else {
                return false;
            }
        }
        
        //
        //    public function isRegisteredUser($phone){
        //        $result = mysql_query("SELECT unique_id FROM users WHERE phone = '$phone' LIMIT 1");
        //        $no_of_rows = mysql_num_rows($result);
        //        if ($no_of_rows > 0) {
        //            return mysql_fetch_assoc($result);
        //        } else {
        //            return false;
        //        }
        //
        //    }
        //
        //    public function updateBusLocation($bus_id, $x, $y){
        //        $result = mysql_query("UPDATE bus SET x ='$x', y = '$y' WHERE unique_id = '$bus_id'");
        //        return $result;
        //
        //    }
        //
        //    public function getBusInfo($bus_id){
        //        $result = mysql_query("SELECT * FROM bus WHERE unique_id = '$bus_id' LIMIT 1");
        //        if($result){
        //            return mysql_fetch_assoc($result);
        //        }
        //        else{
        //            return $result;
        //        }
        //    }
        //
        //    public function getPoints($path_id){
        //        $result = mysql_query("SELECT p.x, p.y, p.name, o.path_order, o.time, o.pass, o.stop FROM point p, point_order o WHERE o.path_id = '$path_id' AND p.unique_id = o.point_id ORDER BY o.path_order");
        //
        //        $no_of_rows = mysql_num_rows($result);
        //        if ($no_of_rows > 0) {
        //            $response = array();
        //            while($row = mysql_fetch_assoc($result)){
        //                $response[] = $row;
        //            }
        //            return $response;
        //        } else {
        //            return false;
        //        }
        //
        //    }
        //
        //    public function getadd1(){
        //        $result = mysql_query("SELECT DISTINCT add1 FROM address");
        //        $no_of_rows = mysql_num_rows($result);
        //        if ($no_of_rows > 0) {
        //            $response = array();
        //            while($row = mysql_fetch_assoc($result)){
        //                $response[] = $row;
        //            }
        //            return $response;
        //        } else {
        //            return false;
        //        }
        //    }
        //
        //    public function getadd2($add1){
        //        $result = mysql_query("SELECT DISTINCT add2 FROM address WHERE add1 = '$add1'");
        //        $no_of_rows = mysql_num_rows($result);
        //        if ($no_of_rows > 0) {
        //            $response = array();
        //            while($row = mysql_fetch_assoc($result)){
        //                $response[] = $row;
        //            }
        //            return $response;
        //        } else {
        //            return false;
        //        }
        //    }
        //
        //    public function getadd3($add1, $add2){
        //        $result = mysql_query("SELECT DISTINCT add3 FROM address WHERE add1 = '$add1' AND add2='$add2'");
        //        $no_of_rows = mysql_num_rows($result);
        //        if ($no_of_rows > 0) {
        //            $response = array();
        //            while($row = mysql_fetch_assoc($result)){
        //                $response[] = $row;
        //            }
        //            return $response;
        //        } else {
        //            return false;
        //        }
        //    }
        //
        //    public function checkduplicate($type, $add1, $add2, $add3, $academy){
        //        $address_id = mysql_query("SELECT unique_id FROM address WHERE add1 = '$add1' AND add2 = '$add2' AND add3 = '$add3'");
        //        $dup = mysql_query("SELECT * FROM academy WHERE type = '$type' AND address_id = '$address_id' AND name = '$academy'");
        //
        //        $no_of_rows = mysql_num_rows($dup);
        //        if ($no_of_rows > 0) {
        //            return true;
        //        } else {
        //            return false;
        //        }
        //    }
        //
        //    public function getaddressid($add1, $add2, $add3){
        //        $address_id = mysql_query("SELECT unique_id FROM address WHERE add1 = '$add1' AND add2 = '$add2' AND add3 = '$add3'");
        //        if($address_id){
        //            return mysql_fetch_assoc($address_id);
        //        }
        //        else{
        //            return false;
        //        }
        //    }
        //
        //    public function saveuserinfo($master, $mobile){
        //        $uuid = uniqid('', true);
        //        $uuid = str_replace('.', '_', $uuid);
        //
        //        $result = mysql_query("INSERT INTO users(unique_id, name, phone) VALUES('$uuid', '$master', '$mobile')");
        //        if($result){
        //            return $uuid;
        //        }
        //        else{
        //            return false;
        //        }
        //    }
        //
        //    public function saveacademyinfo($type, $name, $master, $master_id, $address_id, $address, $students, $phone, $mobile){
        //        $uuid = uniqid('', true);
        //        $uuid = str_replace('.', '_', $uuid);
        //
        //        $result = mysql_query("INSERT INTO academy(unique_id, type, name, master, master_id, address_id, address, num_of_student, phone, mobile) VALUES('$uuid', '$type', '$name', '$master', '$master_id', '$address_id', '$address', '$students', '$phone', '$mobile')");
        //        if($result){
        //            return $uuid;
        //        }
        //        else{
        //            return false;
        //        }
        //    }
        //
        //    public function getAcademyInfo($ac_id){
        //        $result = mysql_query("SELECT * from academy WHERE unique_id = '$ac_id'");
        //        if($result){
        //            return mysql_fetch_assoc($result);
        //        }
        //        else{
        //            return false;
        //        }
        //    }
        //
        //    public function getStaffInfo($user_id){
        //        $result = mysql_query("SELECT s.*, a.name from staff s, academy a WHERE s.user_id = '$user_id' AND s.isuse = 0 AND s.ac_id = a.unique_id");
        //        $no_of_rows = mysql_num_rows($result);
        //        if ($no_of_rows > 0) {
        //            $response = array();
        //            while($row = mysql_fetch_assoc($result)){
        //                $response[] = $row;
        //            }
        //            return $response;
        //        } else {
        //            return false;
        //        }
        //    }
        //
        //    public function getStudentInfo($user_id){
        //        $result = mysql_query("SELECT s.*, a.name from student s, academy a WHERE s.user_id = '$user_id' AND s.isuse = 0 AND s.ac_id = a.unique_id");
        //        $no_of_rows = mysql_num_rows($result);
        //        if ($no_of_rows > 0) {
        //            $response = array();
        //            while($row = mysql_fetch_assoc($result)){
        //                $response[] = $row;
        //            }
        //            return $response;
        //        } else {
        //            return false;
        //        }
        //    }
        //
        //    public function getParentInfo($user_id){
        //        $result = mysql_query("SELECT p.*, a.name from parent p, academy a WHERE p.user_id = '$user_id' AND p.isuse = 0 AND p.ac_id = a.unique_id");
        //        $no_of_rows = mysql_num_rows($result);
        //        if ($no_of_rows > 0) {
        //            $response = array();
        //            while($row = mysql_fetch_assoc($result)){
        //                $response[] = $row;
        //            }
        //            return $response;
        //        } else {
        //            return false;
        //        }
        //    }
        //
        //    public function savestaffinfo($user_id, $type, $ac_id){
        //        $uuid = uniqid('', true);
        //        $uuid = str_replace('.', '_', $uuid);
        //
        //        $result = mysql_query("INSERT INTO staff(unique_id, user_id, type, ac_id) VALUES('$uuid', '$user_id', '$type', '$ac_id')");
        //        if($result){
        //            return $uuid;
        //        }
        //        else{
        //            return false;
        //        }
        //    }
        //
        //    public function updateagree($user_id, $type){
        //        $result = false;
        //        if(type < 4){
        //                //staff
        //            $result = mysql_query("UPDATE staff SET agree = 0, agree_time = NOW() WHERE unique_id = '$user_id'");
        //        }else if(type == 4){
        //                //parent
        //            $result = mysql_query("UPDATE parent SET agree = 0, agree_time = NOW() WHERE unique_id = '$user_id'");
        //        }else{
        //                //students
        //            $result = mysql_query("UPDATE student SET agree = 0, agree_time = NOW() WHERE unique_id = '$user_id'");
        //        }
        //        if($result){
        //            return true;
        //        }
        //        else{
        //            return false;
        //        }
        //    }
        
    }
    
    ?>
