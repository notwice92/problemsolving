<?php

class DB_Connect {

    // constructor
    function __construct() {
        
    }

    // destructor
    function __destruct() {
        // $this->close();
    }

    // Connecting to database
    public function connect() {
        require_once 'config.php';
        // connecting to mysql
        $con = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD) or die ("Don't mysql_connect");
        mysql_query("set names utf8", $con);

		if(!$con)
		{
			die ("Don't mysql_connect #2");
		}
		
        // selecting database
        $db_selected = mysql_select_db(DB_DATABASE) or die ("Don't mysql_select_db");;

		if(!$db_selected)
		{
			die ("Don't mysql_select_db #2");
		}
		
        // return database handler
        return $con;
    }

    // Closing database connection
    public function close() {
        mysql_close();
    }

}

?>
