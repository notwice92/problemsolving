<?php
    header('Content-Type: application/json');
    
    require_once 'include/DB_Functions.php';
    $db = new DB_Functions();
    
    $name = $_POST['name'];
    $email = $_POST['email'];
    $pw = $_POST['pw'];
    
    $response = array("name" => $name, "email" => $email);
    
    $result = $db->register($name, $email, $pw);
    if($result != false){
        echo json_encode($response);
    }else{
        echo "error";
    }
    ?>