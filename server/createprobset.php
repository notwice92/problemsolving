<?php
    header('Content-Type: application/json');
    
    require_once 'include/DB_Functions.php';
    $db = new DB_Functions();
    
    $title = $_POST['title'];
    $user_id = $_POST['user_id'];
    $category_id = $_POST['category_id'];
    
    $response = array("command" => "success");
    
    $result = $db->createset( $title, $user_id, $category_id );
    if($result != false){
        $response["id"] = $result;
        echo json_encode($response);
    }else{
        $response["command"] = "fail";
        echo json_encode($response);
    }
    ?>