<?php
    header('Content-Type: application/json');
    
    require_once 'include/DB_Functions.php';
    $db = new DB_Functions();
    
    $set_id = $_POST['set_id'];
    
    $result = $db->getproblems($set_id);
    if($result != false){
        echo json_encode($result);
    }else{
        echo "error";
    }
    ?>