<?php
    header('Content-Type: application/json');
    
    require_once 'include/DB_Functions.php';
    $db = new DB_Functions();
    
    $set_id = $_POST['set_id'];
    $content = $_POST['content'];
    $img = $_POST['img_path'];
    $sound = $_POST['sound_path'];
    $ans = $_POST['ans'];
    $correct = $_POST['correct'];
    $tags = $_POST['tags'];
    
    $response = array("command" => "success");
    
    $result = $db->createproblem( $set_id, $content, $img, $sound, $ans, $correct, $tags );
    if($result != false){
        echo json_encode($response);
    }else{
        $response["command"] = "fail";
        echo json_encode($response);
    }
    ?>