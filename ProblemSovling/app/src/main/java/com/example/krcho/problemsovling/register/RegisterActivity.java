package com.example.krcho.problemsovling.register;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.krcho.problemsovling.R;

public class RegisterActivity extends AppCompatActivity {
    private EditText name, id, pw;
    private Button send;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mContext = this;

        name = (EditText) findViewById(R.id.et_name);
        id = (EditText) findViewById(R.id.et_id);
        pw = (EditText) findViewById(R.id.et_pw);
        send = (Button) findViewById(R.id.btn_send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isCorrectly()) {
                    register();
                }
            }
        });
    }

    private boolean isCorrectly() {

        //check name
        if (name.getText().toString().equals("")) {
            Toast.makeText(mContext, "이름을 입력해주세요!", Toast.LENGTH_LONG).show();
            return false;
        }

        //check id
        if (id.getText().toString().equals("")) {
            Toast.makeText(mContext, "이메일을 입력해주세요!", Toast.LENGTH_LONG).show();
            return false;
        } else if (!id.getText().toString().contains("@")) {
            Toast.makeText(mContext, "이메일을 입력해주세요!", Toast.LENGTH_LONG).show();
            return false;
        }

        //check pw
        if (pw.getText().toString().equals("")) {
            Toast.makeText(mContext, "비밀번호를 입력해주세요!", Toast.LENGTH_LONG).show();
            return false;
        } else if (pw.getText().toString().length() < 6) {
            Toast.makeText(mContext, "비밀번호는 6자리 이상 입력하세요!", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private void register() {
        Intent data = new Intent();
        data.putExtra("id", id.getText().toString());
        setResult(RESULT_OK, data);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
