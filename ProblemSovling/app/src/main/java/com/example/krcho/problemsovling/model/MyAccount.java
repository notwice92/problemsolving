package com.example.krcho.problemsovling.model;

/**
 * Created by krcho on 2015-11-17.
 */
public class MyAccount {

    private static MyAccount instance;
    private String name, id, pw;

    private MyAccount(){}

    public static MyAccount getInstance(){
        if(instance == null){
            return new MyAccount();
        }
        return instance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPw() {
        return pw;
    }

    public void setPw(String pw) {
        this.pw = pw;
    }
}
