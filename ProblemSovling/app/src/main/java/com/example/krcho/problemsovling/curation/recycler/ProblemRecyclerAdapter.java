package com.example.krcho.problemsovling.curation.recycler;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.example.krcho.problemsovling.model.ProblemSet;

import java.util.ArrayList;

/**
 * Created by krmpr on 15. 11. 18..
 */
public class ProblemRecyclerAdapter extends RecyclerView.Adapter<ProblemRecyclerViewHolder>{
    ArrayList<ProblemSet> list;

    public ProblemRecyclerAdapter(ArrayList<ProblemSet> list) {
        this.list = list;
    }

    @Override
    public ProblemRecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return null;
    }

    @Override
    public void onBindViewHolder(ProblemRecyclerViewHolder problemViewHolder, int i) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }
}
