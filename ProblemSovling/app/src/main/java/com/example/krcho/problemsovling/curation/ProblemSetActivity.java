package com.example.krcho.problemsovling.curation;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.example.krcho.problemsovling.R;
import com.example.krcho.problemsovling.curation.recycler.ProblemRecyclerAdapter;
import com.example.krcho.problemsovling.make.ManageMyProblemSetActivity;
import com.example.krcho.problemsovling.model.ProblemSet;

import java.util.ArrayList;

public class ProblemSetActivity extends AppCompatActivity {

    SwipeRefreshLayout swipeRefreshLayout;
    RecyclerView recyclerView;
    ProblemRecyclerAdapter adapter;
    ArrayList<ProblemSet> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_problem_set);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                refresh();
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        adapter = new ProblemRecyclerAdapter(list);
        recyclerView.setAdapter(adapter);

    }

    public void refresh(){

        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refresh();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_problem_set, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.my:
                startActivity(new Intent(getApplicationContext(), MySolveListActivity.class));
                return true;
            case R.id.manage:
                startActivity(new Intent(getApplicationContext(), ManageMyProblemSetActivity.class));
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
